package com.example.android.rxjavabasketballleague.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.rxjavabasketballleague.R;
import com.example.android.rxjavabasketballleague.activity.LigaActivity;
import com.example.android.rxjavabasketballleague.domain.Liga;

import java.util.List;

public class LigaRecyclerViewAdapter extends RecyclerView.Adapter<LigaRecyclerViewAdapter.ViewHolder> {


    private List<Liga> lista;
    private LayoutInflater layoutInflater;
    Context context;


    // data is passed into the constructor
     public LigaRecyclerViewAdapter(Context context, List<Liga> lista) {
        this.layoutInflater = LayoutInflater.from(context);
        this.lista = lista;
        this.context = context;
    }

    // inflates the row layout from xml when needed

        @Override
         public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View view = layoutInflater.from(parent.getContext()).inflate(R.layout.lista_liga_row, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
         }

        @Override
        public void onBindViewHolder (LigaRecyclerViewAdapter.ViewHolder holder, final int position){
            holder.textViewImeLige.setText(lista.get(position).getIme());
            holder.textViewDatumPocetka.setText(String.valueOf(lista.get(position).getDatumPocetka()));
            holder.textViewDatumZavrsetka.setText(String.valueOf(lista.get(position).getDatumZavrsetka()));
            holder.textViewBrojTimova.setText(String.valueOf(lista.get(position).getBrojTimova()));

            //onClick listener
            holder.parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, LigaActivity.class);
                    intent.putExtra("ligaIme", lista.get(position).getIme());
                    intent.putExtra("datumPocetka", lista.get(position).getDatumPocetka());
                    intent.putExtra("datumZavrsetka", lista.get(position).getDatumZavrsetka());
                    intent.putExtra("brojTimova", lista.get(position).getBrojTimova());
                    context.startActivity(intent);

                }
            });

        }

        @Override
        public int getItemCount () {
            return lista.size();
        }



    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewImeLige, textViewDatumPocetka, textViewDatumZavrsetka, textViewBrojTimova;
        RelativeLayout parentLayout;

        ViewHolder(View itemView) {
            super(itemView);
            this.textViewImeLige = itemView.findViewById(R.id.textViewImeLige);
            this.textViewDatumPocetka = itemView.findViewById(R.id.textViewDatumPocetka);
            this.textViewDatumZavrsetka = itemView.findViewById(R.id.textViewDatumZavrsetka);
            this.textViewBrojTimova = itemView.findViewById(R.id.textViewBrojTimova);
            this.parentLayout = itemView.findViewById(R.id.parentLayoutLiga);

        }
    }


}

