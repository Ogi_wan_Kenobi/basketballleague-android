package com.example.android.rxjavabasketballleague.domain;

public class Tim {

    private Long id;
    private String ime;

    public Tim() {
        super();
    }

    public Tim(Long id, String ime) {
        this.id = id;
        this.ime = ime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }
}
