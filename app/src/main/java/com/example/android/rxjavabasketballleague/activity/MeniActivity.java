package com.example.android.rxjavabasketballleague.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.example.android.rxjavabasketballleague.R;

public class MeniActivity extends AppCompatActivity{

    CardView kreirajLigu, mojeLige;
    TextView logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meni);

        kreirajLigu = findViewById(R.id.kreirajLiguButton);
        mojeLige = findViewById(R.id.mojeLigeButton);
        logout = findViewById(R.id.logoutLink);

        kreirajLigu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MeniActivity.this, KreirajLiguActivity.class);
                MeniActivity.this.startActivity(intent);
            }
        });

        mojeLige.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MeniActivity.this, MojeLigeActivity.class);
                MeniActivity.this.startActivity(intent);
            }
        });


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MeniActivity.this, MainActivity.class);
                MeniActivity.this.startActivity(intent);
            }
        });

    }

}
