package com.example.android.rxjavabasketballleague.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.android.rxjavabasketballleague.RepositoryUtils.MyRepository;
import com.example.android.rxjavabasketballleague.domain.Liga;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

/**
 * Created by demiurg on 18.9.18..
 */
public class MojeLigeViewModel extends ViewModel {

    private MyRepository myRepository;
    private List<Liga> lige;

    private MutableLiveData<List<Liga>> ligeLiveData;

    public MojeLigeViewModel() {
        lige = new ArrayList<>();
        ligeLiveData = new MutableLiveData<>();
        myRepository = new MyRepository();
    }

    public LiveData<List<Liga>> getLige() {
        myRepository.getLige(new SingleObserver<List<Liga>>(){

            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(List<Liga> value) {
                //ovo se desava ako je api uspesan
                //obratiti paznju na to da je ligeLiveData tipa MutableLiveData (vazno je da je LiveData u pitanju)
                ligeLiveData.setValue(value);
                lige = value;
            }

            @Override
            public void onError(Throwable e) {
                ligeLiveData.setValue(null);
            }
        });

        return ligeLiveData;
    }
}
