package com.example.android.rxjavabasketballleague.domain;

import java.util.Date;

public class Liga {

    private Long id;
    private String ime;
    private Date datumPocetka;
    private Date datumZavrsetka;
    private int brojTimova;
    private Long userId;

    public Liga() {

    }

    public Liga(Long id, String ime, Date datumPocetka, Date datumZavrsetka, int brojTimova, Long userId) {
        this.id = id;
        this.ime = ime;
        this.datumPocetka = datumPocetka;
        this.datumZavrsetka = datumZavrsetka;
        this.brojTimova = brojTimova;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public Date getDatumPocetka() {
        return datumPocetka;
    }

    public void setDatumPocetka(Date datumPocetka) {
        this.datumPocetka = datumPocetka;
    }

    public Date getDatumZavrsetka() {
        return datumZavrsetka;
    }

    public void setDatumZavrsetka(Date datumZavrsetka) {
        this.datumZavrsetka = datumZavrsetka;
    }

    public int getBrojTimova() {
        return brojTimova;
    }

    public void setBrojTimova(int brojTimova) {
        this.brojTimova = brojTimova;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
