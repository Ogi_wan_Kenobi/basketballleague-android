package com.example.android.rxjavabasketballleague.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

   // private Long id;
    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("password")
    @Expose
    private String password;

    public User(){
        super();
    }
    public User(String username, String password) {
       // this.id = id;
        this.username = username;
        this.password = password;
    }

//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
