package com.example.android.rxjavabasketballleague.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.rxjavabasketballleague.R;
import com.example.android.rxjavabasketballleague.adapter.TimoviRecyclerViewAdapter;
import com.example.android.rxjavabasketballleague.domain.Tim;

import java.util.ArrayList;
import java.util.List;

public class LigaActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    EditText editTextImeTima;
    ImageView imageViewPlus;
    List<Tim> pomLista = new ArrayList<Tim>();
    int maxBrojTImova;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liga);

        getIncomingIntent();

        //Recyclerview
        recyclerView = findViewById(R.id.timoviRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        imageViewPlus = findViewById(R.id.dodajTimButton);
        editTextImeTima = findViewById(R.id.editTextUnosTima);

        imageViewPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //if(pomLista.size() <= maxBrojTImova){
                if(pomLista.size() < 12){
                    String ime = String.valueOf(editTextImeTima.getText());
                    Tim tim = new Tim(null, ime);
                    pomLista.add(tim);
                    editTextImeTima.setText("");
                    Toast.makeText(getApplicationContext(), ime + " dodat", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getApplicationContext(), "Maksimalan broj timova dostignut!", Toast.LENGTH_LONG).show();
                    editTextImeTima.setText("");
                }
            }
        });



        Tim tim1 = new Tim(Long.valueOf(1), "San Antonio Spurs");
        Tim tim2 = new Tim(Long.valueOf(2), "Los Angeles Lakers");
        Tim tim3 = new Tim(Long.valueOf(3), "Toronto Raptors");
        Tim tim4 = new Tim(Long.valueOf(4), "Boston Celtics");
        Tim tim5 = new Tim(Long.valueOf(5), "Houston Rockets");
        Tim tim6 = new Tim(Long.valueOf(6), "Denver Nugets");
        Tim tim7 = new Tim(Long.valueOf(7), "Indiana Pacers");
        Tim tim8 = new Tim(Long.valueOf(8), "Oklahoma City Thunder");
        Tim tim9 = new Tim(Long.valueOf(9), "Portland Trailblazzers");
        Tim tim10 = new Tim(Long.valueOf(10), "Washington Wizards");



        pomLista.add(tim1);
        pomLista.add(tim2);
        pomLista.add(tim3);
        pomLista.add(tim4);
        pomLista.add(tim5);
        pomLista.add(tim6);
        pomLista.add(tim7);
        pomLista.add(tim8);
        pomLista.add(tim9);
        pomLista.add(tim10);

        displayData(pomLista);

    }

    private void getIncomingIntent(){
        if(getIntent().hasExtra("ligaIme") && getIntent().hasExtra("datumPocetka") && getIntent().hasExtra("datumZavrsetka") && getIntent().hasExtra("brojTimova")){
            String ligaIme = getIntent().getStringExtra("ligaIme");
            String datumPocetka = getIntent().getStringExtra("datumPocetka");
            String datumZavrsetka = getIntent().getStringExtra("datumZavrsetka");
            String brojTimova = getIntent().getStringExtra("brojTimova");

            try {
                maxBrojTImova = Integer.valueOf(brojTimova);
            }catch(Exception e){
                maxBrojTImova = 12;
            }

            setLiga(ligaIme, datumPocetka, datumZavrsetka, brojTimova);
        }
    }

    private void setLiga(String ime, String datumP, String datumZ, String brojT){

        TextView imeLige = findViewById(R.id.textViewLigaImeDetalji);
        TextView datumPocetka = findViewById(R.id.textViewDatumPocetkaDetalji);
        TextView datumZavrsetka = findViewById(R.id.textViewDatumZavrsetkaDetalji );
        TextView brojTimova = findViewById(R.id.textViewBrojTimovaDetalji);

        imeLige.setText(ime);
        datumPocetka.setText(datumP);
        datumZavrsetka.setText(datumZ);
        brojTimova.setText(brojT);
    }

    private void displayData(List<Tim> lista){
        TimoviRecyclerViewAdapter adapter = new TimoviRecyclerViewAdapter(this, lista);
        recyclerView.setAdapter(adapter);
    }

}
