package com.example.android.rxjavabasketballleague.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.rxjavabasketballleague.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class KreirajLiguActivity extends AppCompatActivity {

    CardView unesiButton;
    EditText imeInput, datumPocetkaInput, brojTimovaInput;
    TextView nazadNaMeni;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kreiraj_ligu);

        unesiButton = findViewById(R.id.unesiButton);
        nazadNaMeni = findViewById(R.id.nazadNaMeniLink);
        imeInput = findViewById(R.id.editTextImeLige);
        datumPocetkaInput = findViewById(R.id.editTextDatumPocetka);
        brojTimovaInput = findViewById(R.id.editTextBrojTimova);

        unesiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ime = imeInput.getText().toString();
                String datumPocetkaS = datumPocetkaInput.getText().toString();
                String brojTimovaS = brojTimovaInput.getText().toString();

                if(!ime.isEmpty() && !datumPocetkaS.isEmpty() && !brojTimovaS.isEmpty()){
                    int brojTimova = Integer.parseInt(brojTimovaS);
                    DateFormat format = new SimpleDateFormat("yyyy-mm-dd");
                    try {
                        Date datumPocetka = format.parse(datumPocetkaS);

                        Intent intent = new Intent(KreirajLiguActivity.this, MojeLigeActivity.class);
                        intent.putExtra("msg", "Uspešno kreirana liga!");
                        startActivity(intent);


                    } catch (ParseException e) {
                        Toast.makeText(getApplicationContext(), "Pogrešan format datuma! Pravilno: yyyy-mm-dd",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Polja ne mogu biti prazna!",Toast.LENGTH_LONG).show();
                }
            }
        });

        nazadNaMeni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(KreirajLiguActivity.this, MeniActivity.class);
                KreirajLiguActivity.this.startActivity(intent);
            }
        });

    }
}
