package com.example.android.rxjavabasketballleague.filter;

import com.example.android.rxjavabasketballleague.domain.Liga;

import java.util.ArrayList;
import java.util.List;

public class LigeTransformer {

    private static int myUserId = 1;

    public static List<Liga> transformLige(List<Liga> list) {
        List<Liga> newList = new ArrayList<>();

        for(Liga liga : list) {
            if(liga.getUserId() == myUserId) {
                newList.add(liga);
            }
        }

        return newList;
    }
}
