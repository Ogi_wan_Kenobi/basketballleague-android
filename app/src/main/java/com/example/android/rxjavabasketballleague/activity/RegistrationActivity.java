package com.example.android.rxjavabasketballleague.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.rxjavabasketballleague.R;

public class RegistrationActivity extends AppCompatActivity {

    CardView registerButton;
    TextView loginLink;
    EditText usernameInput, passwordInput, confirmPasswordInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        registerButton  = findViewById(R.id.registerButton);
        loginLink = findViewById(R.id.loginLink);
        usernameInput = findViewById(R.id.editTextUsername);
        passwordInput = findViewById(R.id.editTextPassword);
        confirmPasswordInput = findViewById(R.id.editTextConfirmPassword);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = usernameInput.getText().toString();
                String password = passwordInput.getText().toString();
                String confirmPassword = confirmPasswordInput.getText().toString();

                if(!username.isEmpty() && !password.isEmpty()){
                    if(!username.equals("root")){
                        if(password.equals(confirmPassword)){
                           String msg = "Uspešna registracija!";
                            Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
                            intent.putExtra("msg", msg);
                            RegistrationActivity.this.startActivity(intent);

                        }else{
                            Toast.makeText(getApplicationContext(), "Unete lozinke se ne podudaraju!",Toast.LENGTH_LONG).show();
                            passwordInput.setText("");
                            confirmPasswordInput.setText("");
                        }

                    }else{
                        Toast.makeText(getApplicationContext(), "Korisnik " + username + " već postoji!",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Polja ne mogu biti prazna!",Toast.LENGTH_LONG).show();
                }
            }
        });



        loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
                RegistrationActivity.this.startActivity(intent);
            }
        });

    }
}
