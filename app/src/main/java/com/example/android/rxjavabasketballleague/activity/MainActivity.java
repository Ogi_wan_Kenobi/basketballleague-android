package com.example.android.rxjavabasketballleague.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.rxjavabasketballleague.R;
import com.example.android.rxjavabasketballleague.adapter.LigaRecyclerViewAdapter;
import com.example.android.rxjavabasketballleague.domain.Liga;
import com.example.android.rxjavabasketballleague.domain.User;
import com.example.android.rxjavabasketballleague.retrofit.RetrofitApi;
import com.example.android.rxjavabasketballleague.retrofit.RetrofitClient;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class MainActivity extends AppCompatActivity {

    RetrofitApi retrofitApi;
    CompositeDisposable compositeDisposable;
    CardView loginButton;
    TextView registerLink;
    EditText usernameInput, passwordInput;
    Boolean ABoolean = true;
    User user = new User();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        Retrofit retrofit = new RetrofitClient().getInstance();
//        retrofitApi = retrofit.create(RetrofitApi.class);

        //Primanje toasta
        Intent intent = getIntent();
        String msg = intent.getStringExtra("msg");
        if(msg != null){
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
        }

        loginButton  = findViewById(R.id.loginButton);
        registerLink = findViewById(R.id.registerLink);
        usernameInput = findViewById(R.id.editTextUsername);
        passwordInput = findViewById(R.id.editTextPassword);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = usernameInput.getText().toString();
                String password = passwordInput.getText().toString();
                user.setUsername(username);
                user.setPassword(password);



                if(!username.isEmpty() && !password.isEmpty()){
                   //if(tryLogin(user)){
                       if(!username.isEmpty()){
                       //Toast.makeText(getApplicationContext(), "Dobrodošli " + username,Toast.LENGTH_LONG).show();

                       Intent intent = new Intent(MainActivity.this, MeniActivity.class);
                       MainActivity.this.startActivity(intent);
                       }else{
                       Toast.makeText(getApplicationContext(), "Pogrešan username ili lozinka!",Toast.LENGTH_LONG).show();
                   }
                }else{
                    Toast.makeText(getApplicationContext(), "Polja ne mogu biti prazna!",Toast.LENGTH_LONG).show();
                }

            }
        });


        registerLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegistrationActivity.class);
                MainActivity.this.startActivity(intent);
            }
        });

    }


    public Boolean tryLogin(User user){
        Call<Boolean> call = retrofitApi.login(user);
        call.enqueue(new Callback<Boolean>() {

            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if(response.isSuccessful()){
                    ABoolean = true;
                }else{ABoolean = false;}
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                ABoolean = false;
            }

        });
        return ABoolean;
}




//    private Boolean tryLogin(User user) {
//
//
//        compositeDisposable.add(retrofitApi.login(user)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<Boolean>() {
//                    @Override
//                    public void accept(Boolean aBoolean) throws Exception {
//                        ABoolean = loginResult(aBoolean);
//                    }
//                }));
//
//            return loginResult(ABoolean);
//    }
//
//    private Boolean loginResult(Boolean aBoolean) {
//
//        return aBoolean;
//    }
}
