package com.example.android.rxjavabasketballleague.RepositoryUtils;

import com.example.android.rxjavabasketballleague.domain.Liga;
import com.example.android.rxjavabasketballleague.filter.LigeTransformer;
import com.example.android.rxjavabasketballleague.retrofit.RetrofitApi;
import com.example.android.rxjavabasketballleague.retrofit.RetrofitClient;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by demiurg on 18.9.18..
 */
public class MyRepository {

    private RetrofitApi mRetrofitApi;
    private CompositeDisposable mDisposables;




    public MyRepository() {
        mRetrofitApi = RetrofitClient.createService(RetrofitApi.class);
        mDisposables = new CompositeDisposable();
    }


    public void getLige(SingleObserver<List<Liga>> observer) {
        Single<List<Liga>> observable =
                mRetrofitApi.getLige()
                .map(LigeTransformer::transformLige)
                .doOnSubscribe(disposable -> mDisposables.add(disposable))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        observable.subscribeWith(observer);
    }
}
