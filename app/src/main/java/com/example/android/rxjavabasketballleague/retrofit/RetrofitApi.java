package com.example.android.rxjavabasketballleague.retrofit;

import com.example.android.rxjavabasketballleague.domain.Liga;
import com.example.android.rxjavabasketballleague.domain.User;

import java.util.List;


import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RetrofitApi {

    @POST("User/login")
   // Observable<Boolean> login(@Body User user);
    Call<Boolean> login(@Body User user);

    @GET("Liga/returnAll")
    Single<List<Liga>>getLige();

}
