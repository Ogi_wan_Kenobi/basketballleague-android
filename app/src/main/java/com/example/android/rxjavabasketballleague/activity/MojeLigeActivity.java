package com.example.android.rxjavabasketballleague.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.rxjavabasketballleague.R;
import com.example.android.rxjavabasketballleague.adapter.LigaRecyclerViewAdapter;
import com.example.android.rxjavabasketballleague.domain.Liga;
import com.example.android.rxjavabasketballleague.fragments.MojeLigeFragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MojeLigeActivity extends AppCompatActivity {

    RecyclerView ligaRecyclerView;
    private static final String LIGA_TAG = "LIGA_TAG";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moje_lige);


        Intent intent = getIntent();
        String msg = intent.getStringExtra("msg");
        if(msg != null){
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
        }

        FragmentManager manager = getSupportFragmentManager();

        Fragment fragment = MojeLigeFragment.newInstance();
            manager.beginTransaction().add(fragment, LIGA_TAG).commit();
        //RecyclerView
//        ligaRecyclerView = findViewById(R.id.listaLigaRecycler);
//        ligaRecyclerView.setHasFixedSize(true);
//        ligaRecyclerView.setLayoutManager(new LinearLayoutManager(this));
//
//        //Test
//
//        String string = "2018-09-17";
//        DateFormat format = new SimpleDateFormat("yyyy-mm-dd");
//        Date date1 = new Date();
//        try {
//            date1 = format.parse(string);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        Liga liga1 = new Liga(null, "Liga1", date1, date1, 12, Long.valueOf(1));
//        Liga liga2 = new Liga(null, "Liga2", date1, date1, 12, Long.valueOf(1));
//        Liga liga3 = new Liga(null, "Liga3", date1, date1, 12, Long.valueOf(1));
//        Liga liga4 = new Liga(null, "Liga4", date1, date1, 12, Long.valueOf(1));
//
//        List<Liga> pomLista = new ArrayList<>();
//        pomLista.add(liga1);
//        pomLista.add(liga2);
//        pomLista.add(liga3);
//        pomLista.add(liga4);
//        Log.i("LISTA:    ", pomLista.get(0).getIme());
//        Log.i("LISTA:    ", pomLista.get(1).getIme());
//        Log.i("LISTA:    ", pomLista.get(2).getIme());
//
//        displayData(pomLista);
        //Test


    }

    private void displayData(List<Liga> lista){
        LigaRecyclerViewAdapter adapter = new LigaRecyclerViewAdapter(this, lista);
        ligaRecyclerView.setAdapter(adapter);
    }

}
