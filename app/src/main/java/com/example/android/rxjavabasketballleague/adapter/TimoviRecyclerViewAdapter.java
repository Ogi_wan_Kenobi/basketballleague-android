package com.example.android.rxjavabasketballleague.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.rxjavabasketballleague.R;
import com.example.android.rxjavabasketballleague.domain.Tim;

import java.util.List;

public class TimoviRecyclerViewAdapter extends RecyclerView.Adapter<TimoviRecyclerViewAdapter.ViewHolder> {

    private List<Tim> lista;
    private LayoutInflater layoutInflater;
    Context context;

    public TimoviRecyclerViewAdapter(Context context, List<Tim> lista) {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.lista = lista;

    }

    @Override
    public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View view = layoutInflater.from(parent.getContext()).inflate(R.layout.lista_timova_row, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        int redniBroj = position + 1;
        holder.textViewImeTima.setText(redniBroj + ". "+ lista.get(position).getIme());
        holder.removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ime = lista.get(position).getIme();
                lista.remove(position);
                Toast.makeText(context,  ime + " obrisan!", Toast.LENGTH_LONG).show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewImeTima;
        LinearLayout parentLayout;
        ImageView removeButton;

        ViewHolder(View itemView) {
            super(itemView);
            this.textViewImeTima = itemView.findViewById(R.id.imeTima);
            this.removeButton = itemView.findViewById(R.id.removeButton);
            this.parentLayout = itemView.findViewById(R.id.parentLayoutTim);

        }
    }


}
