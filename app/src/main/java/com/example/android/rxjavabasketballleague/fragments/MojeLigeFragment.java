package com.example.android.rxjavabasketballleague.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.android.rxjavabasketballleague.R;
import com.example.android.rxjavabasketballleague.adapter.LigaRecyclerViewAdapter;
import com.example.android.rxjavabasketballleague.domain.Liga;
import com.example.android.rxjavabasketballleague.viewmodels.MojeLigeViewModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by demiurg on 18.9.18..
 */
public class MojeLigeFragment extends Fragment {

    private MojeLigeViewModel mojeLigeViewModel;
    private LigaRecyclerViewAdapter adapter;
    RecyclerView ligaRecyclerView;

    public static MojeLigeFragment newInstance() {
        return new MojeLigeFragment ();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_moje_lige, container, false);

        ligaRecyclerView = view.getRootView().findViewById(R.id.listaLigaRecycler);

        adapter = new LigaRecyclerViewAdapter(getContext(), getMokapLige());
        ligaRecyclerView.setAdapter(adapter);

        ligaRecyclerView.setHasFixedSize(true);
        ligaRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mojeLigeViewModel = ViewModelProviders.of(this).get(MojeLigeViewModel.class);

        mojeLigeViewModel.getLige().observe(this, new Observer<List<Liga>>() {
            @Override
            public void onChanged(@Nullable List<Liga> ligas) {
                updateUi(ligas); //funkcija koja te iste live podatke lepi na prikaz.
            }
        });

        return view;
    }

    private void updateUi(List<Liga> lige) {
        if(lige != null) {
            adapter = new LigaRecyclerViewAdapter(getContext(), lige);
            ligaRecyclerView.setAdapter(adapter);
        } else {
            adapter = new LigaRecyclerViewAdapter(getContext(), getMokapLige());
            ligaRecyclerView.setAdapter(adapter);
        }
    }

    private List<Liga> getMokapLige() {
        List<Liga> lige = new ArrayList<>();

        Liga liga1 = new Liga(null, "Liga1", new Date(), new Date(), 12, 1L);
        Liga liga2 = new Liga(null, "Liga2", new Date(), new Date(), 12, 1L);
        Liga liga3 = new Liga(null, "Liga3", new Date(), new Date(), 12, 1L);
        Liga liga4 = new Liga(null, "Liga4", new Date(), new Date(), 12, 1L);

        lige.add(liga1);
        lige.add(liga2);
        lige.add(liga3);
        lige.add(liga4);

        return lige;
    }
}
